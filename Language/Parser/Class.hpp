#pragma once
#include <string>
#include <vector>
#include <map>
#include "../Lexer/Lexer.hpp"
#include "Function.hpp"
#include <llvm/IR/Type.h>
#include <llvm/IR/TypeBuilder.h>
#include <llvm/IR/Module.h>

using namespace std;
using namespace llvm;

namespace Compiler {

	class Namespace;
	class Class;
	class Class : public std::enable_shared_from_this<Class> {
		string name;

		map<string, string> fields;
		vector<shared_ptr<Function>> methods;

		llvm::StructType* type;

	public:

		Class(string name) : name(name) {
		}


		void Dump() {
			printf("class %s {\n", name.c_str());
			for (auto field : fields) {
				printf("%s %s;\n", field.second.c_str(), field.first.c_str());
			}
			for (auto meth : methods) {
				meth->Dump();
			}
			printf("}\n");
		}


		string GetName() { return name; }


		bool HasField(string name) {
			return fields.count(name) == 1;
		}


		void AddField(string type, string name) {
			fields[name] = type;
		}


		void AddMethod(shared_ptr<Function>& func) {
			methods.push_back(func);
			func->SetAsMethod(shared_from_this());
		}

		vector<shared_ptr<Function>>& GetMethods() {
			return methods;
		}

		shared_ptr<Function> GetMethod(string name) {
			for (auto meth : methods) {
				if (meth->GetName() == name) {
					return meth;
				}
			}
			throw runtime_error("Method doesn't exist: " + name);
		}

		void CreateType(Module* module);
		void GenerateType(shared_ptr<Namespace> root, Module* module);

		virtual Type* GetType() {
			return type;
		}
	};

	class IntClass : public Class {
	public:
		IntClass() : Class("int") {}
		virtual Type* GetType() {
			return IntegerType::get(getGlobalContext(), 32);
		}
	};

	class VoidClass : public Class {
	public:
		VoidClass() : Class("int") {}
		virtual Type* GetType() {
			return Type::getVoidTy(getGlobalContext());
		}
	};
}