#pragma once

#include <string>
#include <vector>
#include <sstream>
#include "../Lexer/Lexer.hpp"
#include "Expression.hpp"

#include <llvm\IR\Module.h>
#include <llvm\IR\LLVMContext.h>
#include <llvm\IR\Function.h>

using namespace std;
using namespace llvm;

namespace Compiler {

	class Class;
	class Namespace;

	class Function {
		string name;

		string returnType;
		vector<Lexer::Token>::iterator start;
		vector<pair<string, string>> arguments;

		vector<shared_ptr<Expression>> body;

		shared_ptr<Class> owner;

		llvm::Function* function;

	public:

		Function(string name) :
			name(name), returnType("void") {
		}


		llvm::Function* GetFunction() {
			return function;
		}


		void SetAsMethod(shared_ptr<Class>& type) {
			owner = type;
		}


		vector<Lexer::Token>::iterator GetStart() {
			return start;
		}


		void AddArgument(string type, string name) {
			arguments.push_back(make_pair(name, type));
		}


		void SetStart(vector<Lexer::Token>::iterator openBrace) {
			start = openBrace;
		}


		string& GetName() {
			return name;
		}


		vector<pair<string, string>>& GetArguments() {
			return arguments;
		}


		void SetReturnType(string type) {
			returnType = type;
		}


		void AddStatement(shared_ptr<Expression>& expr) {
			body.push_back(expr);
		}


		void Dump() {
			stringstream  s;

			for (uint32_t i = 0; i < arguments.size(); ++i) {
				s << arguments[i].second << " " << arguments[i].first;
				if (i < arguments.size() - 1) {
					s << ", ";
				}
			}

			printf("%s (%s) -> %s {", name.c_str(), s.str().c_str(), returnType.c_str());

			stringstream code;

			for (auto expr : body) {
				code << "\n" << expr->Dump() << ";";

			}

			printf("%s\n}\n", code.str().c_str());
		}


		string GetIRName();

		void GenerateBody(shared_ptr<Namespace> root, Module* module);

	};

}