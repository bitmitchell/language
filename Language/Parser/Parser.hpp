#pragma once

#include <string>
#include <vector>
#include <iterator>
#include <stdexcept>
#include <sstream>
#include <memory>
#include "../Lexer/Lexer.hpp"

#include "Namespace.hpp"
#include "Expression.hpp"

using namespace std;

namespace Compiler {

	class Parser {
	public:

		class ParseError : public runtime_error {
		public:
			ParseError(Lexer::Token& error, string expected) :
				runtime_error("Parser Error at: " + to_string(error.Line) + ":" +
				to_string(error.Position) + "(" + error.Text + "), expected " + expected) {
			}
		};

		class NameError : public runtime_error {
		public:
			NameError(Lexer::Token& error, string reason) :
				runtime_error("Name Error at: " + to_string(error.Line) + ":" +
				to_string(error.Position) + ", " + reason) {
			};
		};

		typedef vector<Lexer::Token>::iterator TokenIterator;

		bool TokenMatches(Lexer::Token& token, string text, Lexer::Type type);
		void FindMatchingBrace(TokenIterator& start);


		shared_ptr<Namespace> ScanNamespace(TokenIterator& it);
		shared_ptr<Class> ScanClass(TokenIterator& it);
		void ParseFunction(shared_ptr<Function>& func);
		shared_ptr<Expression> ParseExpression(TokenIterator& it, string end = ";");

		void ParseFunctions(shared_ptr<Namespace>& space);

	};
}