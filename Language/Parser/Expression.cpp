#include "Expression.hpp"

using namespace Compiler;


llvm::Value* Identifier::CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope) {
	return scope->Resolve(name);
}


llvm::Value* Value::CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope>) {
	return llvm::ConstantInt::get(llvm::IntegerType::get(llvm::getGlobalContext(), 32), value, 10);
}


llvm::Value* Neg::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	return builder.CreateNeg(expr->CodeGen(builder, scope));
}


llvm::Value* Add::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	return builder.CreateAdd(left->CodeGen(builder, scope), right->CodeGen(builder, scope));
}


llvm::Value* Sub::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	return builder.CreateSub(left->CodeGen(builder, scope), right->CodeGen(builder, scope));
}


llvm::Value* Mul::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	return builder.CreateMul(left->CodeGen(builder, scope), right->CodeGen(builder, scope));
}


llvm::Value* Div::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	return builder.CreateSDiv(left->CodeGen(builder, scope), right->CodeGen(builder, scope));
}


llvm::Value* Pow::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	throw std::runtime_error("Power operator not yet implemented");
}


llvm::Value* Return::CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) {
	builder.CreateRet(expr->CodeGen(builder, scope));
	return nullptr;
}