#pragma once
#include <string>
#include <vector>
#include <sstream>
#include "../Lexer/Lexer.hpp"
#include "Class.hpp"
#include "Parser.hpp"

using namespace std;

namespace Compiler {

	class Namespace {
		string name;

		vector<shared_ptr<Namespace>> namespaces;
		vector<shared_ptr<Class>> classes;

	public:

		Namespace(string name) : name(name) {
		}


		void AddNamespace(shared_ptr<Namespace>& space) {
			namespaces.push_back(space);
		}


		void AddClass(shared_ptr<Class>& type) {
			classes.push_back(type);
		}


		vector<shared_ptr<Class>>& GetClasses() {
			return classes;
		}


		vector<shared_ptr<Namespace>>& GetNamespaces() {
			return namespaces;
		}


		shared_ptr<Class> GetClass(string name) {
			if (name == "void") {
				return shared_ptr<Class>(new VoidClass());
			} else if (name == "int") {
				return shared_ptr<Class>(new IntClass());
			}

			for (auto& type : classes) {
				if (type->GetName() == name) {
					return type;
				}
			}
			return shared_ptr<Class>();
		}


		void Dump() {
			printf("namespace %s {\n", name.c_str());
			for (shared_ptr<Namespace> space : namespaces) {
				space->Dump();
			}
			for (shared_ptr<Class> type : classes) {
				type->Dump();
			}
			printf("}\n");
		}


		void CreateTypes(Module* module) {
			for (shared_ptr<Namespace> space : namespaces) {
				space->CreateTypes(module);
			}
			for (shared_ptr<Class> type : classes) {
				type->CreateType(module);
			}
		}


		void GenerateTypes(shared_ptr<Namespace> root, Module* modele) {
			for (shared_ptr<Namespace> space : namespaces) {
				space->GenerateTypes(root, modele);
			}
			for (shared_ptr<Class> type : classes) {
				type->GenerateType(root, modele);
			}
		}
	};

}