#include "Lexer.hpp"

#include <fstream>

using namespace Compiler;

string Lexer::LoadFile(string filename) {
	string line, source;
	ifstream file(filename);

	while (file.good()) {
		getline(file, line);
		source += line + "\n"; // add the line of code onto the existing source
	}

	file.close();
	return source;
}


Lexer::Type Lexer::CharType(char c) {
	if (isalpha(c))
		return Type::Text;
	else if (isdigit(c))
		return Type::Number;
	else if (isspace(c))
		return Type::Whitespace;
	else
		return Type::Symbol;
}


bool Lexer::TypesMatch(char first, char second) {
	Type firstType = CharType(first);
	Type secondType = CharType(second);
	
	if (firstType == Type::Number && second == '.') {
		return true;
	}

	if (firstType == Type::Symbol || firstType == Type::Whitespace) {
		return false;
	}

	return firstType == secondType;
}


void Lexer::Process(string filename, back_insert_iterator<vector<Token>> iter) {

	string source = LoadFile(filename);

	size_t index = 0;

	while (index < source.length() && CharType(source[index]) == Type::Whitespace) {
		++index;
	}

	while (index < source.length()) {
		int start = index;
		Type type = CharType(source[index++]);

		while (index < source.length() && TypesMatch(source[start], source[index])) index++;

		if (type != Whitespace)
		{
			size_t lastNewLine = source.rfind("\n", start);
			size_t charPos = start - lastNewLine - 1;

			Token t(source.substr(start, index - start),
				type,
				count(source.begin(), source.begin() + index, '\n') + 1,
				charPos);
			*iter++ = t;
		}
	}

}