#pragma once
#include <memory>
#include <map>
#include <llvm\IR\Value.h>

using namespace std;

namespace Compiler {

	/*
		Scope's handle namespace value resolution for the llvm code gen phase
		*/
	class Scope {
		shared_ptr<Scope> parent;

		map<string, llvm::Value*> variables;

	public:

		Scope(shared_ptr<Scope>& parent = shared_ptr<Scope>(nullptr)) : parent(parent) {}

		void AddValue(string name, llvm::Value* value) {
			if (variables.count(name)) {
				throw runtime_error("Variable with that name already exists");
			}
			variables[name] = value;
		}

		llvm::Value* Resolve(string name) {
			if (variables.count(name)) {
				return variables[name];
			} else {
				if (parent) {
					return parent->Resolve(name);
				} else {
					throw runtime_error("Variable with that name does not exist");
				}
			}
		}


	};

}