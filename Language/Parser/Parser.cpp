#include "Parser.hpp"
#include "Function.hpp"
#include <stack>
#include <functional>

// TODO: This file is quite messy, large functions, bad use of lambdas, FIX ME

using namespace std;
using namespace Compiler;

bool Parser::TokenMatches(Lexer::Token& token, string text, Lexer::Type type) {
	return token.Text == text && token.Type == type;
}


void Parser::FindMatchingBrace(TokenIterator& start) {
	int count(1);

	while (count) {
		++start;

		if (TokenMatches(*start, "{", Lexer::Symbol)) {
			++count;
		} else if (TokenMatches(*start, "}", Lexer::Symbol)) {
			--count;
		}
	}
}


shared_ptr<Namespace> Parser::ScanNamespace(Parser::TokenIterator& it) {
	// expect a 'namespace' Name { THINGS }
	if (!TokenMatches(*it, "namespace", Lexer::Text)) {
		throw ParseError(*it, "namespace");
	}

	++it;

	if (it->Type != Lexer::Text) {
		throw ParseError(*it, "identifier");
	}

	shared_ptr<Namespace> space(new Namespace(it->Text));

	++it;

	if (!TokenMatches(*it, "{", Lexer::Symbol)) {
		throw ParseError(*it, "{");
	}

	++it;

	while (!TokenMatches(*it, "}", Lexer::Symbol)) {
		// until we find a closing brace, scan other things

		if (TokenMatches(*it, "namespace", Lexer::Text)) {

			space->AddNamespace(ScanNamespace(it));

		} else if (TokenMatches(*it, "class", Lexer::Text)) {

			space->AddClass(ScanClass(it));

		} else if (TokenMatches(*it, "function", Lexer::Text)) {

			throw ParseError(*it, "nothing, function not yet supported");

		} else {
			throw ParseError(*it, "namespace, class, or function");
		}

	}


	return space;	
}


shared_ptr<Class> Parser::ScanClass(TokenIterator& it) {
	// expect a 'class' Name { THINGS }
	if (!TokenMatches(*it, "class", Lexer::Text)) {
		throw ParseError(*it, "class");
	}

	++it;

	if (it->Type != Lexer::Text) {
		throw ParseError(*it, "identifier");
	}

	shared_ptr<Class> type(new Class(it->Text));

	++it;

	if (!TokenMatches(*it, "{", Lexer::Symbol)) {
		throw ParseError(*it, "{");
	}

	++it;

	while (!TokenMatches(*it, "}", Lexer::Symbol)) {
		Lexer::Token& first = *it++;

		if (first.Type != Lexer::Text) {
			throw ParseError(*it, "field or method");
		}
		
		if (TokenMatches(*it, "(", Lexer::Symbol)) {
			
			shared_ptr<Function> func(new Function(first.Text));
			++it;

			while (!TokenMatches(*it, ")", Lexer::Symbol)) {
				Lexer::Token& first = *it++;
				Lexer::Token& second = *it++;

				if (first.Type != Lexer::Text) {
					throw ParseError(first, "type");
				}
				if (second.Type != Lexer::Text) {
					throw ParseError(second, "parameter name");
				}

				func->AddArgument(first.Text, second.Text);

				if (TokenMatches(*it, ",", Lexer::Symbol)) {
					++it;
				}
			}

			++it;

			if (TokenMatches(*it, "-", Lexer::Symbol)) {
				++it;
				if (TokenMatches(*it, ">", Lexer::Symbol)) {
					++it;
					if (it->Type != Lexer::Text) {
						throw ParseError(*it, "return type");
					}
					func->SetReturnType(it->Text);
					++it;
				} else {
					throw ParseError(*it, ">");
				}
			}

			if (!TokenMatches(*it, "{", Lexer::Symbol)) {
				throw ParseError(*it, "{");
			}

			func->SetStart(it);

			FindMatchingBrace(it);

			type->AddMethod(func);
			++it;

		} else {
			// its a type definition
			if (!type->HasField(it->Text)) {
				type->AddField(first.Text, it->Text);
			} else {
				throw NameError(*it, "field with this name already exists");
			}

			++it;
			if (!TokenMatches(*it, ";", Lexer::Symbol)) {
				throw ParseError(*it, "semicolon after field");
			}
			++it;
		}
	}

	return type;
}


void Parser::ParseFunctions(shared_ptr<Namespace>& space) {
	for (shared_ptr<Class>& type : space->GetClasses()) {
		for (shared_ptr<Function>& func : type->GetMethods()) {
			ParseFunction(func);
		}
	}

	for (shared_ptr<Namespace>& recur : space->GetNamespaces()) {
		ParseFunctions(recur);
	}
}


void Parser::ParseFunction(shared_ptr<Compiler::Function>& func) {
	TokenIterator it = func->GetStart();

	if (!TokenMatches(*it, "{", Lexer::Symbol)) {
		throw ParseError(*it, "start or a function");
	}
	
	++it;

	while (!TokenMatches(*it, "}", Lexer::Symbol)) {
		// check for keywords
		if (TokenMatches(*it, "return", Lexer::Text)) {
			func->AddStatement(shared_ptr<Expression>(new Return(ParseExpression(++it))));
		} else {
			func->AddStatement(ParseExpression(it));
		}

		++it;
	}
}


shared_ptr<Expression> Parser::ParseExpression(TokenIterator& it, string end) {
	// HACK: Please ignore the copious amount of lambdas used here, its a WIP

	typedef stack<shared_ptr<Expression>> ExpressionStack;
	ExpressionStack operators;
	ExpressionStack operands;

	string binaryOperators = "+-*/^";
	string unaryOperators = "-";

	function<void(ExpressionStack&, ExpressionStack&)> E, P;

	auto pop = [](ExpressionStack& stack) {
		auto v = stack.top();
		stack.pop();
		return v;
	};

	auto next = [&]() {
		return *(it);
	};

	auto consume = [&]() {
		++it; 
	};

	auto expect = [&](string text, Lexer::Type type) {
		if (TokenMatches(next(), text, type)) {
			consume();
		} else {
			throw ParseError(next(), text);
		}
	};

	auto binary = [&](Lexer::Token& tok) {
		if (tok.Text == "+") {
			return shared_ptr<Expression>(new Add());
		} else if (tok.Text == "-") {
			return shared_ptr<Expression>(new Sub());
		} else if(tok.Text == "/") {
			return shared_ptr<Expression>(new Div());
		} else if (tok.Text == "*") {
			return shared_ptr<Expression>(new Mul());
		} else if (tok.Text == "^") {
			return shared_ptr<Expression>(new Pow());
		}
		throw ParseError(tok, "Binary Operator");		
	};

	auto unary = [&](Lexer::Token& tok) {
		if (tok.Text == "-") {
			return shared_ptr<Expression>(new Neg());
		}
		throw ParseError(tok, "Unary Operator");
	};

	auto popOperator = [&](ExpressionStack& operators, ExpressionStack& operands) {
		if (operators.top()->OperandCount() == 2) {
			auto op = dynamic_pointer_cast<BinaryExpression>(pop(operators));
			auto t1 = pop(operands);
			auto t0 = pop(operands);
			op->SetOperands(t0, t1);
			operands.push(op);
		} if (operators.top()->OperandCount() == 1) {
			auto op = dynamic_pointer_cast<UnaryExpression>(pop(operators));
			op->SetOperand(pop(operands));
			operands.push(op);
		}
	};

	auto pushOperator = [&](shared_ptr<Expression>& op, ExpressionStack& operators, ExpressionStack& operands) {
		while (operators.size() && operators.top()->GetPrecedence() > op->GetPrecedence()) {
			popOperator(operators, operands);
		}
		operators.push(op);
	};

	E = [&](ExpressionStack& operators, ExpressionStack& operands) {
		P(operators, operands);
		while (binaryOperators.find(next().Text) != binaryOperators.npos) {
			pushOperator(binary(next()), operators, operands);
			consume();
			P(operators, operands);
		}

		while (operators.top()->OperandCount()) {
			popOperator(operators, operands);
		}
	};

	P = [&](ExpressionStack& operators, ExpressionStack& operands) {
		if (next().Type == Lexer::Number) {
			operands.emplace(new Value(next().Text));
			consume();
		} else if (next().Type == Lexer::Text) {
			Lexer::Token& id = next();
			consume();
			if (TokenMatches(next(), "(", Lexer::Symbol)) {
				consume();
				shared_ptr<FunctionCall> call(new FunctionCall(id.Text));
				while (!TokenMatches(next(), ")", Lexer::Symbol)) {
					call->AddArgument(ParseExpression(it, ",)"));
					if (TokenMatches(next(), ",", Lexer::Symbol)) {
						consume();
					}
				}
				operands.emplace(call);
				consume();
			} else {
				operands.emplace(new Identifier(id.Text));
			}

		} else if (TokenMatches(next(), "(", Lexer::Symbol)) {
			consume();
			operators.emplace(new Sentinel());
			E(operators, operands);
			expect(")", Lexer::Symbol);
			operators.pop();
		} else if (unaryOperators.find(next().Text) != unaryOperators.npos) {
			pushOperator(unary(next()), operators, operands);
			consume();
			P(operators, operands);
		} else {
			ParseError(next(), "valid expression");
		}
	};

	operators.emplace(new Sentinel());
	E(operators, operands);
	if (end.find(next().Text) == end.npos) {
		throw ParseError(next(), "one of " + end);
	}

	return operands.top();
}