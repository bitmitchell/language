#pragma once
#include <memory>
#include <sstream>
#include <llvm/IR/Verifier.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include "Scope.hpp"
using namespace std;

namespace Compiler {

	class Expression {
	public:
		virtual int GetPrecedence() { return 0; }
		virtual int OperandCount() { return 0; }
		virtual string Dump() { return ""; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope) { return nullptr; }
	};


	class Sentinel : public Expression {};


	class Identifier : public Expression {
		string name;
	public:
		Identifier(string name) : name(name) {}
		virtual string Dump() { return name; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Value : public Expression {
		string value;
	public:
		Value(string value) : value(value) {}
		virtual string Dump() { return value; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class BinaryExpression : public Expression {
	protected:
		shared_ptr<Expression> left, right;
	public:
		void SetOperands(shared_ptr<Expression> l, shared_ptr<Expression> r) {
			left = l;
			right = r;
		}
		virtual int OperandCount() { return 2; }
	};


	class UnaryExpression : public Expression {
	protected:
		shared_ptr<Expression> expr;
	public:
		void SetOperand(shared_ptr<Expression> operand) { expr = operand; }
		virtual int OperandCount() { return 1; }
	};


	class Neg : public UnaryExpression {
	public:
		virtual int GetPrecedence() { return 80; }
		virtual string Dump() { return "(-" + expr->Dump() + ")"; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Add : public BinaryExpression {
	public:
		virtual int GetPrecedence() { return 50; }
		virtual string Dump() { return "(" + left->Dump() + " + " + right->Dump() + ")"; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Sub : public BinaryExpression {
	public:
		virtual int GetPrecedence() { return 50; }
		virtual string Dump() { return "(" + left->Dump() + " - " + right->Dump() + ")"; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Mul : public BinaryExpression {
	public:
		virtual int GetPrecedence() { return 70; }
		virtual string Dump() { return "(" + left->Dump() + " * " + right->Dump() + ")"; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Div : public BinaryExpression {
	public:
		virtual int GetPrecedence() { return 70; }
		virtual string Dump() { return "(" + left->Dump() + " / " + right->Dump() + ")"; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Pow : public BinaryExpression {
	public:
		virtual int GetPrecedence() { return 90; }
		virtual string Dump() { return "(" + left->Dump() + " ^ " + right->Dump() + ")"; }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>&, shared_ptr<Scope> scope);
	};


	class Return : public Expression {
		shared_ptr<Expression> expr;
	public:
		Return(shared_ptr<Expression>& expr) : expr(expr) {}
		virtual string Dump() { return "return " + expr->Dump(); }
		virtual llvm::Value* CodeGen(llvm::IRBuilder<>& builder, shared_ptr<Scope> scope);
	};


	class FunctionCall : public Expression {
		string function;
		vector<shared_ptr<Expression>> arguments;
	public:
		FunctionCall(string func) : function(func) {}
		void AddArgument(shared_ptr<Expression>& expr) {
			arguments.push_back(expr);
		}
		virtual string Dump() {
			stringstream s;
			s << function << "(";
			for (uint32_t i = 0; i < arguments.size(); ++i) {
				s << arguments[i]->Dump();
				if (i < arguments.size() - 1) {
					s << ", ";
				}
			}
			s << ")";
			return s.str();
		}
	};

}