#pragma once

#include <string>
#include <vector>
#include <iterator>

using namespace std;

namespace Compiler {

	/*
		The Lexer simply takes an input string (source file) and outputs a list of tokens.
	*/
	class Lexer {
	public:
		enum Type {
			Text,
			Number,
			Symbol,
			Whitespace,
		};

		struct Token {
			string Text;
			Type Type;
			int Line;
			int Position;

			Token(string text, Lexer::Type type, int line, int position) :
				Text(text), Type(type), Line(line), Position(position) {
			}
		};

		void Process(string filename, back_insert_iterator<vector<Token>> iter);

	private:

		string LoadFile(string filename);
		Type CharType(char character);
		bool TypesMatch(char first, char second);

	};

}