#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <functional>
#include <string>
#include <memory>
#include "Lexer\Lexer.hpp"
#include "Parser\Parser.hpp"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/SectionMemoryManager.h>
#include <llvm/ExecutionEngine/JIT.h>
#include <llvm/Support/TargetSelect.h>

#pragma comment(lib, "LLVMX86Disassembler")
#pragma comment(lib, "LLVMX86AsmParser")
#pragma comment(lib, "LLVMX86CodeGen")
#pragma comment(lib, "LLVMSelectionDAG")
#pragma comment(lib, "LLVMAsmPrinter")
#pragma comment(lib, "LLVMX86Desc")
#pragma comment(lib, "LLVMObject")
#pragma comment(lib, "LLVMMCParser")
#pragma comment(lib, "LLVMBitReader")
#pragma comment(lib, "LLVMX86Info")
#pragma comment(lib, "LLVMX86AsmPrinter")
#pragma comment(lib, "LLVMX86Utils")
#pragma comment(lib, "LLVMJIT")
#pragma comment(lib, "LLVMExecutionEngine")
#pragma comment(lib, "LLVMCodeGen")
#pragma comment(lib, "LLVMScalarOpts")
#pragma comment(lib, "LLVMInstCombine")
#pragma comment(lib, "LLVMTransformUtils")
#pragma comment(lib, "LLVMipa")
#pragma comment(lib, "LLVMAnalysis")
#pragma comment(lib, "LLVMTarget")
#pragma comment(lib, "LLVMMC")
#pragma comment(lib, "LLVMCore")
#pragma comment(lib, "LLVMSupport")

using namespace llvm;

void Execute(Module* module);

int main(int c, char** v) {

	Compiler::Lexer lexer;
	vector<Compiler::Lexer::Token> tokens;

	for (int i = 1; i < c; ++i) {
		lexer.Process(v[i], back_inserter(tokens));
	}

	tokens.emplace_back("EOF", Compiler::Lexer::Whitespace, -1, -1);

	shared_ptr<Compiler::Namespace> assembly;

	Compiler::Parser parser;
	try {
		assembly = parser.ScanNamespace(tokens.begin());
		parser.ParseFunctions(assembly);
		assembly->Dump();
	} catch (Compiler::Parser::ParseError e) {
		printf("%s\n", e.what());
	} catch (Compiler::Parser::NameError e) {
		printf("%s\n", e.what());
	}

	Module* module = new Module("Assembly", getGlobalContext());

	assembly->CreateTypes(module);
	assembly->GenerateTypes(assembly, module);

	module->dump();

	Execute(module);

	system("pause");

	return 0;
}


void Execute(Module* module) {

	InitializeNativeTarget();

	ExecutionEngine* engine = ExecutionEngine::createJIT(module);

	Function* func = engine->FindFunctionNamed("_Foo_DoSomething");
	void* funcPtr = engine->getPointerToFunction(func);
	std::function<int(void*, int)> _Foo_DoSomething(reinterpret_cast<int(*)(void*,int)>(funcPtr));

	int ret = _Foo_DoSomething(nullptr, 5);

	printf("_Foo_DoSomething returned %i!\n", ret);

}