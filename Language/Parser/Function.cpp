#include "Function.hpp"
#include "Class.hpp"
#include "Namespace.hpp"
#include "Scope.hpp"

#include <llvm/IR/IRBuilder.h>

using namespace Compiler;


string Compiler::Function::GetIRName() 
{
	stringstream n;
	if (owner) {
		n << "_" << owner->GetName() << "_";
	}
	n << name;
	return n.str();
}



void Compiler::Function::GenerateBody(shared_ptr<Namespace> root, Module* module) {
	string irName = GetIRName();

	shared_ptr<Class> returnClass = root->GetClass(returnType);

	shared_ptr<Scope> scope(new Scope());

	// build the argument list
	vector<llvm::Type*> args = { owner->GetType()->getPointerTo() };

	for (auto arg : arguments) {
		args.push_back(root->GetClass(arg.second)->GetType());
	}
	

	llvm::FunctionType* funcType = llvm::FunctionType::get(returnClass->GetType(), args, false);
	function = llvm::Function::Create(funcType, llvm::Function::ExternalLinkage, irName, module);

	auto it = function->arg_begin();
	scope->AddValue("this", it);
	for (auto arg : arguments) {
		++it;
		scope->AddValue(arg.first, it);
	}

	BasicBlock* block = BasicBlock::Create(getGlobalContext(), "entry", function);
	IRBuilder<> builder(getGlobalContext());
	builder.SetInsertPoint(block);

	for (shared_ptr<Expression>& expr : body) {
		expr->CodeGen(builder, scope);
	}
}