#include "Class.hpp"
#include <llvm\IR\LLVMContext.h>
#include <llvm\IR\Type.h>
#include <llvm\IR\TypeBuilder.h>
#include <llvm\IR\Module.h>
#include "Namespace.hpp"

using namespace Compiler;
using namespace llvm;


void Class::CreateType(Module* module) {
	type = llvm::StructType::create(module->getContext(), name);
}


void Class::GenerateType(shared_ptr<Namespace> root, Module* module) {
	vector<llvm::Type*> elements;
	for (auto field : fields) {
		shared_ptr<Class> type = root->GetClass(field.second);
		if (type) {
			elements.emplace_back(type->GetType());
		} else {
			throw runtime_error("Type not found: " + field.second);
		}
	}

	type->setBody(elements);

	for (auto method : methods) {
		method->GenerateBody(root, module);
	}
}